<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = \App\Event::all();
        // For sidebar in Layout.layout
        $eventAll = \App\Event::all();

        return view('index_event', compact('events','eventAll'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $events = \App\Event::all();
        // For sidebar in Layout.layout
        $eventAll = \App\Event::all();
        return view('create_event',compact('events', 'eventAll'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $event = new \App\Event;
        $event->title = $request->get('title');
        $event->save();

        return redirect('events')->with('success','Event successfully added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $events = \App\Event::find($id);
        // For sidebar in Layout.layout
        $eventAll = \App\Event::all();
        $participants = \App\Participant::all()->where('event_id', $events->id);

        return view('show_event', compact('participants', 'events', 'eventAll'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = \App\Event::find($id);
        // For sidebar in Layout.layout
        $eventAll = \App\Event::all();
        return view('edit_event', compact('event', 'id', 'eventAll'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $event = \App\Event::find($id);
        $event->title = $request->get('title');
        $event->save();
        return redirect('events')->with('success','Event updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = \App\Event::find($id);
        $event->delete();
        return redirect('events')->with('success', 'Event deleted');
    }
}
