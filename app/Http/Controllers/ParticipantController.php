<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ParticipantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $event = \App\Event::find($id);
        // For sidebar in Layout.layout
        $eventAll = \App\Event::all();
        return view('create_participant', compact('event','eventAll'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $participant = new \App\Participant;
        $participant->name = $request->get('name');
        $participant->ic = $request->get('ic');
        $participant->phone_number = $request->get('phone_number');
        $participant->email = $request->get('email');
        $participant->event_id = $request->get('event_id');
        $participant->save();

        return redirect('events/'.$request->get('event_id'))->with('success','Participant Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $participant = \App\Participant::find($id);
        // For sidebar in Layout.layout
        $eventAll = \App\Event::all();
        return view('edit_participant', compact('participant', 'id','eventAll'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $participant = \App\Participant::find($id);
        $participant->name = $request->get('name');
        $participant->ic = $request->get('ic');
        $participant->phone_number = $request->get('phone_number');
        $participant->email = $request->get('email');
        $participant->event_id = $request->get('event_id');
        $participant->save();

        return redirect('events/'.$request->get('event_id'))->with('success', 'Participant successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $participant = \App\Participant::find($id);
        $participant->delete();

        return redirect('events/'.$participant->event_id)->with('success', 'Participant succesfully deleted');
    }
}
