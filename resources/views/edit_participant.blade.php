@extends('layout.layout')

@section('page_title')
  Edit Participant
@endsection

@section('content')
  <form method="post" action="{{action('ParticipantController@update', $id)}}" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="_method" value="PATCH">
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="Name">Name</label>
            <input type="text" class="form-control" name="name" value="{{$participant->name}}">
          </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="ic">IC Number</label>
            <input type="text" class="form-control" name="ic" value="{{$participant->ic}}">
          </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="number">Phone Number</label>
            <input type="text" class="form-control" name="phone_number" value="{{$participant->phone_number}}">
          </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="email">E-mail</label>
            <input type="text" class="form-control" name="email" value="{{$participant->email}}">
          </div>
        </div>
        <input type="text" name="event_id" value="{{$participant->event_id}}" hidden>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <button type="submit" class="btn btn-success w-100">Submit</button>
          </div>
        </div>
      </form>
@endsection
