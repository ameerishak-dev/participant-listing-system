@extends('layout.layout')

@section('page_title')
  Event List
@endsection

@section('content')
  @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
     @endif
    <table class="table table-striped">
    <thead>
      <tr>
        <th>Event Name</th>
        <th colspan="2">Action</th>
      </tr>
    </thead>
    <tbody>

      @foreach($events as $event)
      <tr>
        <td>
          <a href="{{action('EventController@show', $event['id'])}}">{{$event['title']}}</a>
        </td>
        <td>
          <a href="{{action('EventController@edit', $event['id'])}}" class="btn btn-warning">Edit</a>

          <form class="d-inline-block" action="{{action('EventController@destroy', $event['id'])}}" method="post">
            @csrf
            <input name="_method" type="hidden" value="DELETE">
            <button class="btn btn-danger" type="submit">Delete</button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
@endsection
