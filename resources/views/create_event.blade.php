@extends('layout.layout')

@section('page_title')
  Add New Event
@endsection

@section('content')
  <form method="post" action="{{url('events')}}" enctype="multipart/form-data">
        @csrf
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="Title">Event title:</label>
            <input type="text" class="form-control" name="title">
          </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <button type="submit" class="btn btn-success w-100">Submit</button>
          </div>
        </div>
      </form>
@endsection
