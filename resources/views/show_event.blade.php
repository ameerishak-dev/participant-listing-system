@extends('layout.layout')

@section('page_title')
  {{$events->title}}
@endsection

@section('content')
  @if (\Session::has('success'))
      <div class="alert alert-success">
        <p>{{ \Session::get('success') }}</p>
      </div><br />
     @endif

     <a class="float-right mb-4 mr-4" href="/participants/create/{{$events->id}}" style="font-size:30px;"><i class="fa fa-plus-square"></i></a>

    <table class="table table-striped">
    <thead>
      <tr>
        <th>Participant Name</th>
        <th>IC Number</th>
        <th>Phone Number</th>
        <th>E-mail</th>
        <th colspan="2">Action</th>
      </tr>
    </thead>
    <tbody>

      @foreach($participants as $participant)
      <tr>
        <td>{{$participant['name']}}</td>
        <td>{{$participant['ic']}}</td>
        <td>{{$participant['phone_number']}}</td>
        <td>{{$participant['email']}}</td>
        <td>
          <a href="{{action('ParticipantController@edit', $participant['id'])}}" class="btn btn-warning">Edit</a>

          <form class="d-inline-block" action="{{action('ParticipantController@destroy', $participant['id'])}}" method="post">
            @csrf
            <input name="_method" type="hidden" value="DELETE">
            <button class="btn btn-danger" type="submit">Delete</button>
          </form>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
@endsection
