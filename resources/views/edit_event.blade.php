@extends('layout.layout')

@section('page_title')
  Edit Event
@endsection

@section('content')
  <form method="post" action="{{action('EventController@update', $id)}}" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="_method" value="PATCH">
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="Title">Event title:</label>
            <input type="text" class="form-control" name="title" value="{{$event->title}}">
          </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <button type="submit" class="btn btn-success w-100">Submit</button>
          </div>
        </div>
      </form>
@endsection
