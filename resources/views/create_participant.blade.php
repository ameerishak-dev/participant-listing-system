@extends('layout.layout')

@section('page_title')
  Add New Participant
@endsection

@section('content')
  <form method="post" action="{{url('participants')}}" enctype="multipart/form-data">
        @csrf
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="Name">Name</label>
            <input type="text" class="form-control" name="name">
          </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="ic">IC Number</label>
            <input type="text" class="form-control" name="ic">
          </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="number">Phone Number</label>
            <input type="text" class="form-control" name="phone_number">
          </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="email">E-mail</label>
            <input type="text" class="form-control" name="email">
          </div>
        </div>
        <input type="text" name="event_id" value="{{$event->id}}" hidden>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <button type="submit" class="btn btn-success w-100">Submit</button>
          </div>
        </div>
      </form>
@endsection
